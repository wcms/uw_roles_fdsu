<?php

/**
 * @file
 * uw_roles_fdsu.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function uw_roles_fdsu_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 8,
  );

  // Exported role: content author.
  $roles['content author'] = array(
    'name' => 'content author',
    'weight' => 12,
  );

  // Exported role: content editor.
  $roles['content editor'] = array(
    'name' => 'content editor',
    'weight' => 11,
  );

  // Exported role: site manager.
  $roles['site manager'] = array(
    'name' => 'site manager',
    'weight' => 9,
  );

  // Exported role: form editor.
  $roles['form editor'] = array(
    'name' => 'form editor',
    'weight' => 13,
  );

  // Exported role: form results access.
  $roles['form results access'] = array(
    'name' => 'form results access',
    'weight' => 14,
  );

  // Exported role: special alerter.
  $roles['special alerter'] = array(
    'name' => 'special alerter',
    'weight' => 15,
  );

  // Exported role: award content author.
  $roles['award content author'] = array(
    'name' => 'award content author',
    'weight' => 17,
  );

  // Exported role: emergency alerter.
  $roles['emergency alerter'] = array(
    'name' => 'emergency alerter',
    'weight' => 16,
  );

  // Exported role: WCMS support.
  $roles['WCMS support'] = array(
    'name' => 'WCMS support',
    'weight' => 10,
  );

  // Exported role: uWaterloo faculty.
  $roles['uWaterloo faculty'] = array(
    'name' => 'uWaterloo faculty',
    'weight' => 5,
  );

  // Exported role: uWaterloo staff.
  $roles['uWaterloo staff'] = array(
    'name' => 'uWaterloo staff',
    'weight' => 6,
  );

  // Exported role: uWaterloo student.
  $roles['uWaterloo student'] = array(
    'name' => 'uWaterloo student',
    'weight' => 7,
  );

  // Exported role: uWaterloo alumni.
  $roles['uWaterloo alumni'] = array(
    'name' => 'uWaterloo alumni',
    'weight' => 3,
  );

  // Exported role: WCMS web services.
  $roles['WCMS web services'] = array(
    'name' => 'WCMS web services',
    'weight' => 20,
  );

  // Exported role: notices editor.
  $roles['notices editor'] = array(
    'name' => 'notices editor',
    'weight' => 18,
  );

  // Exported role: uWaterloo employer.
  $roles['uWaterloo employer'] = array(
    'name' => 'uWaterloo employer',
    'weight' => 4,
  );

  // Exported role: events editor.
  $roles['events editor'] = array(
    'name' => 'events editor',
    'weight' => 19,
  );

  // Exported role: private content viewer.
  $roles['private content viewer'] = array(
    'name' => 'private content viewer',
    'weight' => 2,
  );

  return $roles;
}
