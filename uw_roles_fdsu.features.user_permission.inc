<?php

/**
 * @file
 * uw_roles_fdsu.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_roles_fdsu_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer role expire'.
  $permissions['administer role expire'] = array(
    'name' => 'administer role expire',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'role_expire',
  );

  return $permissions;
}
